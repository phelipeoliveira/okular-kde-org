---
date: 2011-01-26
title: Okular 0.12 julkaistiin
---
Okularin versio 0.12 on julkaistu KDE:n sovellusten 4.6-julkaisuversiossa. Tämä julkaisuversio esittelee pieniä korjauksia sekä ominaisuuksia, ja on suositeltu päivitys kaikille Okularin käyttäjille.
