---
date: 2009-06-03
title: Okular 0.8.4 julkaistiin
---
Okular 0.8.4 on julkaistu KDE 4.2-sarjan neljännessä korjausjulkaisussa. Tähän julkaisuversioon kuuluu joitakin korjauksia OpenDocument Text -asiakirjojen käsittelyyn, muutamia kaatumiskorjauksia sekä muutamia käyttöliittymäkorjauksia. Voit lukea kaikista korjatuista ongelmista osoitteessa <a href="http://www.kde.org/announcements/changelogs/changelog4_2_3to4_2_4.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_3to4_2_4.php</a>
