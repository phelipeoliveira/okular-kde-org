---
date: 2015-04-15
title: Okular 0.22 julkaistiin
---
Okularin versio 0.22 on julkaistu KDE Applications 15.04 -julkaisuversiossa. Okular 0.22 on suositeltu päivitys kaikille Okularin käyttäjille.
