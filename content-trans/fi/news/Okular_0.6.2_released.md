---
date: 2008-03-05
title: Okular 0.6.2 julkaistiin
---
Okular 0.6.2 on julkaistu KDE 4.0-sarjan toisessa korjausjulkaisussa. Tähän julkaisuversioon kuuluu aika monta virhekorjausta kuten suurempi vakaus suljettaessa asiakirjoja sekä pieniä korjauksia kirjanmerkkijärjestelmään. Voit lukea kaikista korjatuista ongelmista osoitteessa <a href="http://www.kde.org/announcements/changelogs/changelog4_0_1to4_0_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0_1to4_0_2.php</a>
