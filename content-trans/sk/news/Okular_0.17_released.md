---
date: 2013-08-16
title: Vydaný Okular 0.17
---
The 0.17 version of Okular has been released together with KDE Applications 4.11 release. This release introduces new features like undo/redo support for forms and annotations and configurable review tools. Okular 0.17 is a recommended update for everyone using Okular.
