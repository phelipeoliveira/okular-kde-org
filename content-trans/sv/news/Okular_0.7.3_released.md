---
date: 2008-11-04
title: Okular 0.7.3 utgiven
---
Den tredje underhållsutgåvan i KDE 4.1-serien inkluderar Okular 0.7.3. Den innehåller några mindre rättningar av användargränssnittet och i textsökningen. Man kan läsa alla rättade problem på <a href="http://www.kde.org/announcements/changelogs/changelog4_1_2to4_1_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1_2to4_1_3.php</a>
