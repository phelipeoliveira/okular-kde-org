---
date: 2021-04-22
title: Okular 21.04 utgiven
---
Version 21.04 av Okular har givits ut. Utgåvan introducerar digital signering av PDF-filer och diverse mindre felrättningar och funktionsförbättringar överallt. Du kan titta på den fullständiga ändringsloggen på <a href='https://kde.org/announcements/changelogs/releases/21.04.0/#okular'>https://kde.org/announcements/changelogs/releases/21.04.0/#okular</a>. Okular 21.04 är en rekommenderad uppdatering för alla som använder Okular.
