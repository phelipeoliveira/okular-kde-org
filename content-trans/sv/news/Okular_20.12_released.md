---
date: 2020-12-10
title: Okular 20.12 utgiven
---
Version 20.12 av Okular har givits ut. Utgåvan introducerar diverse mindre rättningar och funktioner överallt. Du kan titta på den fullständiga ändringsloggen på <a href='https://kde.org/announcements/changelog-releases.php?version=20.12.0#okular'>https://kde.org/announcements/changelog-releases.php?version=20.12.0#okular</a>. Okular 20.12 är en rekommenderad uppdatering för alla som använder Okular.
