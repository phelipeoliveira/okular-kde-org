---
date: 2006-11-02
title: Okular 0.5.81 instabil förutgåva utgiven
---
Okular-gruppen presenterar med stolthet förutgåvan av Okular som går att kompilera med <a href="http://dot.kde.org/1162475911/">andra förutgåvan av KDE 4 för utvecklare</a>. Förutgåvan har inte ännu fullständig funktionalitet, eftersom vi har mycket att polera och slutföra, men prova den gärna och ge så mycket återmatning som du vill. Paketet med förutgåvan finns på <a href="ftp://ftp.kde.org/pub/kde/unstable/apps/KDE4.x/graphics/okular-0.5.81.tar.bz2">ftp://ftp.kde.org/pub/kde/unstable/apps/KDE4.x/graphics/okular-0.5.81.tar.bz2</a>. Ta en titt på <a href="download.php">nerladdningssidan</a> för att försäkra dig om att du har biblioteken som krävs. 
