---
date: 2013-08-16
title: Okular 0.17 utgiven
---
Version 0.17 av Okular har givits ut tillsammans med utgåva 4.11 av KDE:s program. Utgåvan introducerar nya funktioner som stöd för ångra/gör om i formulär, kommentarer och inställningsbara granskningsverktyg. Okular 0.17 är en rekommenderad uppdatering för alla som använder Okular.
