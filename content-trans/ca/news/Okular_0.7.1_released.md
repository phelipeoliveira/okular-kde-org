---
date: 2008-09-03
title: Publicat l'Okular 0.7.1
---
La primera publicació de manteniment de les sèries 4.1 del KDE conté l'Okular 0.7.1. Inclou diverses esmenes de fallades entre altres esmenes petites. Podeu llegir tots els problemes solucionats a <a href="http://www.kde.org/announcements/changelogs/changelog4_1to4_1_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1to4_1_1.php</a>
