---
date: 2014-12-17
title: Publication de la version 0.21 d'Okular
---
La version 0.21 d'Okular a été livrée avec la version 14.12 des applications KDE. Cette version intègre des nouvelles fonctionnalités comme la prise en charge de la recherche inversée en dvi et des corrections de bugs mineurs. Okular 0.21 est une mise à jour recommandée pour tout utilisateur d'Okular.
