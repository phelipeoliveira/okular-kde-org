---
date: 2009-06-03
title: Publication de la version 0.8.4 d'Okular
---
La version 0.8.4 d'Okular a été livrée avec les applications fournies avec la quatrième version de maintenance de la famille de KDE 4.2. Elle corrige des problèmes dans les documents de format « OpenDocument », un ensemble de corrections de plantage et quelques erreurs mineures dans l'interface. Vous pouvez prendre connaissance de tous les problèmes corrigés à l'adresse <a href="http://www.kde.org/announcements/changelogs/changelog4_2_3to4_2_4.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_3to4_2_4.php</a>
