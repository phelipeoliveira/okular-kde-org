---
intro: Okular prend en charge une grande variété de formats de documents et de cas
  d'utilisations. Cette page se réfère toujours à des séries stables de Okular, actuellement,
  Okular 20.12.
layout: formats
menu:
  main:
    name: Format du document
    parent: about
    weight: 1
sassFiles:
- /sass/table.scss
title: État des gestionnaires de formats de documents
---
