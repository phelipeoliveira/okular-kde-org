---
date: 2015-12-16
title: Publicouse Okular 0.24
---
A versión 0.24 de Okular publicouse coa versión 15.12 das aplicacións de KDE 15.12.0. Esta versión introduce pequenas funcionalidades e solucións de erros, pode revisar o rexistro de cambios completo en <a href='https://www.kde.org/announcements/fulllog_applications-15.12.0.php#okular'>https://www.kde.org/announcements/fulllog_applications-15.12.0.php#okular</a>. Okular 0.24 é unha actualización recomendada para calquera que use Okular.
