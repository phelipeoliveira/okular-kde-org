---
date: 2008-10-03
title: Publicouse Okular 0.7.2
---
A segunda versión de mantemento da serie 4.1 de KDE inclúe Okular 0.7.2. Inclúe algunhas pequenas correccións nas infraestruturas de TIFF e Comicbook e o cambio a «Axustar á anchura» como nivel de ampliación predeterminado. Pode consultar todos os problemas solucionados en <a href="http://www.kde.org/announcements/changelogs/changelog4_1_1to4_1_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1_1to4_1_2.php</a>
