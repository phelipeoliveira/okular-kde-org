---
date: 2008-03-05
title: Publicouse Okular 0.6.2
---
A segunda versión de mantemento da serie 4.0 de KDE inclúe Okular 0.6.2. Inclúe bastantes correccións de fallos, incluída unha mellora da estabilidade ao pechar un documento, e pequenas correccións no sistema de marcadores. Pode consultar todos os problemas solucionados en <a href="http://www.kde.org/announcements/changelogs/changelog4_0_1to4_0_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0_1to4_0_2.php</a>
