---
date: 2009-03-04
title: Publicouse Okular 0.8.1
---
A primeira versión de mantemento da serie KDE 4.2 inclúe Okular 0.8.1. Inclúe correccións de quebras nas infraestruturas de CHM e DjVu e pequenas correccións de erros na interface de usuario. Pode ler todos os problemas solucionados en <a href="http://www.kde.org/announcements/changelogs/changelog4_2to4_2_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2to4_2_1.php</a>
