---
date: 2008-11-04
title: Okular 0.7.3 wydany
---
Trzecie wydanie podtrzymujące linię KDE 4.1 obejmuje Okular 0.7.3. Zawiera ono drobne poprawki interfejsu użytkownika oraz dodaje wyszukiwanie tekstu. Listę wszystkich zmian można znaleźć na <a href="http://www.kde.org/announcements/changelogs/changelog4_1_2to4_1_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1_2to4_1_3.php</a>
