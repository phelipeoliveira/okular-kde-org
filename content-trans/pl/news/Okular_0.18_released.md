---
date: 2013-12-12
title: Okular 0.18 wydany
---
Wersja 0.18 Okulara została wydana wraz z wydaniem Aplikacji do KDE 4.12. Wydanie to wprowadza funkcje takie jak obsługa dźwięku/obrazu w plikach EPub, a także usprawnienia do istniejących funkcji takich jak znajdywanie i drukowanie. Okular 0.18 jest zalecanym uaktualnieniem dla wszystkich go używających.
