---
date: 2014-12-17
title: O Okular 0.21 foi lançado
---
A versão 0.21 do Okular foi lançada junto com a versão 14.12 do KDE Applications. Esta versão introduz novas funcionalidades, como suporte para pesquisa inversa em latex-synctex para arquivos DVI, assim como pequenas correções. É uma atualização recomendada a todos os usuários do Okular.
