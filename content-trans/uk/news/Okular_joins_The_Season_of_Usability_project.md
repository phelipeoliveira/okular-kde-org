---
date: 2007-01-31
title: Okular приєднався до проєкту Season of Usability
---
Команда розробників Okular з гордістю оголошує про те, що Okular було обрано серед інших програм для участі у проєкті <a href="http://www.openusability.org/season/0607/">Season of Usability</a>, який підтримується знавцями з дизайну зі спілки <a href="http://www.openusability.org">OpenUsability</a>. Ми хотіли б привітати нового учасника команди, Sharad Baliyan, та подякувати Florian Graessle та Pino Toscano за їхню невтомну працю з покращення Okular.
