---
date: 2014-12-17
title: Vydán Okular 0.21
---
The 0.21 version of Okular has been released together with KDE Applications 14.12 release. This release introduces new features like latex-synctex reverse searching in dvi and small bugfixes. Okular 0.21 is a recommended update for everyone using Okular.
