---
date: 2015-04-15
title: Vydán Okular 0.22
---
The 0.22 version of Okular has been released together with KDE Applications 15.04 release. Okular 0.22 is a recommended update for everyone using Okular.
