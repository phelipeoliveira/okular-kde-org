---
date: 2010-08-10
title: Vydán Okular 0.11
---
The 0.11 version of Okular has been released together with KDE Applications 4.5 release. This release introduce small fixes and features and is a recommended update for everyone using Okular.
