---
date: 2020-12-10
title: Okular 20.12 vrijgegeven
---
De 20.12 versie van Okular is vrijgegeven. Deze vrijgave introduceert verschillende kleine reparaties en functies. U kunt de volledige log met wijzigingen bekijken op <a href='https://kde.org/announcements/changelog-releases.php?version=20.12.0#okular'>https://kde.org/announcements/changelog-releases.php?version=20.12.0#okular</a>. Okular 20.12 wordt aanbevolen voor iedereen die Okular gebruikt.
