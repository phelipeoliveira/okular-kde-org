---
date: 2020-04-23
title: Okular 1.10 vrijgegeven
---
De 1.10 versie van Okular is vrijgegeven. Deze vrijgave introduceert kinetisch schuiven, verbeteringen aan tabbladen, verbeteringen aan de mobiele UI en overal verschillende kleine reparaties en functies. U kunt de volledige log met wijzigingen bekijken op <a href='https://kde.org/announcements/changelog-releases.php?version=20.04.0#okular'>https://kde.org/announcements/changelog-releases.php?version=20.04.0#okular</a>. Okular 1.10 wordt aanbevolen voor iedereen die Okular gebruikt.
