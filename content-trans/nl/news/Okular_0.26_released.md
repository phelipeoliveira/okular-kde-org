---
date: 2016-08-18
title: Okular 0.26 vrijgegeven
---
De 0.26 versie van Okular is vrijgegeven samen met de vrijgave van KDE Applications 16.08. Deze vrijgave introduceert zeer kleine wijzigingen, u kunt de volledige log met wijzigingen bekijken op <a href='https://www.kde.org/announcements/fulllog_applications.php?version=16.08.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=16.08.0#okular</a>. Okular 0.26 wordt aanbevolen aan iedereen die Okular gebruikt.
