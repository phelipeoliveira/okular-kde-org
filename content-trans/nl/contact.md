---
menu:
  main:
    parent: about
    weight: 4
title: Contact
---
<img src="/images/konqi-dev.png" align="right" alt="Konqi, de KDE mascotte" style="height: 200px;"/>

U kunt met het team van Okular op vele manieren in contact komen:

* E-maillijst: om ontwikkeling van okular te coördineren gebruiken we de [e-maillijst okular-devel](https://mail.kde.org/mailman/listinfo/okular-devel) op kde.org. U kunt het gebruiken om te praten over de ontwikkeling van de hoofdtoepassing en terugkoppeling geven over bestaande of nieuwe backends wordt gewaardeerd.

* IRC: voor algemeen chatten gebruiken we de IRC [#okular](irc://irc.kde.org/#okular) en [#kde-devel](irc://irc.kde.org/#kde-devel) op het [Freenode network](http://www.freenode.net/). Sommige ontwikkelaars van Okular hangen hier rond.

* Matrix: tot de eerder genoemde chat kan ook over het Matrix network via [#okular:kde.org](https://matrix.to/#/#okular:kde.org) toegang geven.

* Forum: als u de voorkeur geeft aan een forum dan kunt u [Okular-forum](http://forum.kde.org/viewforum.php?f=251) in het grotere [Forums van de KDE gemeenschap](http://forum.kde.org/) gebruiken.

* Bugs en wensen: bugs en wensen zouden gerapporteerd moeten worden in de [KDE bugvolger](http://bugs.kde.org).  Als u wilt helpen dan kunt u een lijst met belangrijke bugs [hier](https://community.kde.org/Okular) vinden.
