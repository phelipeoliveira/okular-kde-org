---
date: 2015-12-16
title: O Okular 0.24 foi lançado
---
A versão 0.24 do Okular foi lançada em conjunto com a versão 15.12 das Aplicações do KDE. Esta versão introduz pequenas correcções de erros e funcionalidades, podendo verificar o registo de alterações completo em <a href='https://www.kde.org/announcements/fulllog_applications-15.12.0.php#okular'>https://www.kde.org/announcements/fulllog_applications-15.12.0.php#okular</a>. É uma actualização recomendada para todos os que usam o Okular.
