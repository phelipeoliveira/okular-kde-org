---
date: 2017-12-14
title: O Okular 1.3 foi lançado
---
A versão 1.3 do Okular foi lançada em conjunto com a versão 17.12 das Aplicações do KDE. Esta versão introduz alterações na forma como as anotações e os dados dos formulários funcionam, suporta as actualizações parciais do desenho para os ficheiros que demoram bastante tempo a carregar, torna as ligações de texto interactivas no modo de selecção de texto, adiciona uma opção para Partilhar no menu Ficheiro, adiciona o suporte de Markdown e corrige alguns problemas que dizem respeito ao suporte para HiDPI. Poderá verificar o registo de alterações completo em <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.12.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.12.0#okular</a>. O Okular 1.3 é uma actualização recomendada para todos os que usam o Okular.
