---
date: 2008-04-02
title: O Okular 0.6.3 foi lançado
---
A terceira versão de manutenção da série KDE 4.0 inclui o Okular 0.6.3. Inclui algumas correcções de erros, i.e., uma forma melhor de obter a posição do texto num documento PDF, assim como algumas correcções no sistema de anotações e no índice. Poderá ler todas as questões corrigidas em <a href="http://www.kde.org/announcements/changelogs/changelog4_0_2to4_0_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0_2to4_0_3.php</a>
