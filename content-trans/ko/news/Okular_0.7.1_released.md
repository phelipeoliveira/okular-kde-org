---
date: 2008-09-03
title: Okular 0.7.1 출시
---
KDE 4.1 시리즈의 첫 번째 유지 관리 릴리스에는 Okular 0.7.1이 들어 있습니다. 이 릴리스에는 사소한 충돌 수정 및 여러 작은 변경 사항이 들어 있습니다. 전체 변경 사항을 보려면 <a href="http://www.kde.org/announcements/changelogs/changelog4_1to4_1_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1to4_1_1.php</a> 페이지를 방문하십시오
