---
date: 2010-02-09
title: Okular 0.10 출시
---
Okular의 0.10 버전은 KDE SC 4.4와 함께 출시되었습니다. 이 릴리스에서는 전반적인 안정성 개선 외에도 LaTeX 소스 코드와 DVI/PDF 파일간의 역탐색 및 전방향 탐색 링크 지원 등이 향상되었습니다.
