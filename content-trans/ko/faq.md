---
faq:
- answer: Ubuntu (thus Kubuntu as well) packages of Okular are compiled without support
    for these two formats. The reason is explained in [this](https://bugs.launchpad.net/kdegraphics/+bug/277007)
    Launchpad report.
  question: When using Ubuntu, I cannot read CHM and EPub documents, even if I have
    okular-extra-backends and libchm1 installed. Why?
- answer: Because you don't have a speech service on your system. Install the Qt Speech
    library and they should be enabled
  question: Why are the speak options in the Tools menu grayed out?
- answer: poppler-data 패키지 설치
  question: 일부 문자가 표시되지 않으며 디버그를 활성화하면 'XXX 언어의 언어 팩이 없습니다'와 같은 메시지가 표시됩니다
layout: faq
menu:
  main:
    parent: about
    weight: 5
sassFiles:
- /sass/faq.scss
title: 자주 묻는 질문
---
