---
date: 2015-08-19
title: Izšel je Okular 0.23
---
Različica Okular 0.23 je bila izdana skupaj s KDE Applications 15.08. Ta izdaja uvaja podporo za mehke prehode v predstavitvenem načinu, pa tudi nekatere popravke v zvezi z zaznamki in predvajanjem videoposnetkov. Okular 0.23 je priporočljiva posodobitev za vse, ki uporabljajo Okular.
