---
date: 2009-03-04
title: Izšel je Okular 0.8.1
---
Prva izdaja za vzdrževanje serije KDE 4.2 vključuje Okular 0.8.1. Vključuje nekaj popravkov izpadov pri CHM in DjVu, ter manjše popravke v uporabniškem vmesniku. Vse odpravljene težave si lahko preberete na strani <a href="http://www.kde.org/announcements/changelogs/changelog4_2to4_2_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2to4_2_1.php</a>
