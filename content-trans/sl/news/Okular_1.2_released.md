---
date: 2017-08-17
title: Izšel je Okular 1.2
---
Različica 1.2 Okular je bila izdana skupaj s KDE Applications 17.08. Ta izdaja predstavlja manjše popravke in izboljšave. Celoten dnevnik sprememb lahko preverite na <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.08.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.08.0#okular</a>. Okular 1.2 je priporočljiva posodobitev za vse, ki uporabljajo Okular.
