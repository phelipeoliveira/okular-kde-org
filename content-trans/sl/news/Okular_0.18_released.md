---
date: 2013-12-12
title: Izšel je Okular 0.18
---
Različica 0.18 programa Okular je bila izdana skupaj s programi KDE Izdaja 4.12. Ta izdaja uvaja nove funkcije, kot je avdio/video podpora v datotekah EPub in tudi izboljšave obstoječih funkcij, kot sta iskanje in tiskanje. Okular 0.18 je priporočljiva posodobitev za vse, ki uporabljajo Okular.
