---
date: 2006-08-27
title: Izšel je Okular 0.5 kot nestabilni prikaz
---
Ekipa Okular s ponosom objavlja delovno izdajo programa Okular, ki se prevaja skupaj z verzijo <a href="http://dot.kde.org/1155935483/">KDE 4 'Krash' snapshot</a>. Ta delovna verzija še ni v celoti funkcionalna, saj je potrebno še veliko stvari zgladiti in dokončati, a imate priložnost za preizkušanje in nudenje povratnih informacij kolikor želite. Delovno verzijo paketa lahko najdete na <a href="ftp://ftp.kde.org/pub/kde/unstable/apps/KDE4.x/graphics/okular-0.5.tar.bz2">ftp://ftp.kde.org/pub/kde/unstable/apps/KDE4.x/graphics/okular-0.5.tar.bz2</a>. Poglejte na stran <a href="download.php">download</a>, da se prepričate, da imate vse potrebne knjižnice.
