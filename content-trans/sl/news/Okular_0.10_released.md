---
date: 2010-02-09
title: Izšel je Okular 0.10
---
Različica Okular 0.10 je bila izdana skupaj s KDE SC 4.4. Poleg splošnih izboljšav stabilnosti ima ta izdaja novo/izboljšano podporo za iskanje nazaj in iskanje naprej, ki povezuje vrstice izvorne kode iz lateksa z ustreznimi lokacijami v datotekah dvi in pdf.
