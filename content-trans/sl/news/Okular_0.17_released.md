---
date: 2013-08-16
title: Izšel je Okular 0.17
---
Različica Okular 0.17 je bila izdana skupaj s programi KDE izdaja 4.11. Ta izdaja uvaja nove funkcije, kot je podpora za razveljavitev/uveljavitev zaobrazce in opombe ter nastavljiva orodja za pregled. Okular 0.17 je priporočena posodobitev za vse, ki uporabljajo Okular.
