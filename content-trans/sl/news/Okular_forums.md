---
date: 2012-10-10
title: Okular forumi
---
Sedaj obstaja podporum znotraj foruma Skupnosti KDE. Lahko ga najdete na <a href="http://forum.kde.org/viewforum.php?f=251">http://forum.kde.org/viewforum.php?f=251</href>.
