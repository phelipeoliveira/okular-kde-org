---
date: 2013-02-06
title: Ilmus Okular 0.16
---
Okulari versioon 0.16 ilmus koos KDE rakenduste väljalaskega 4.10. See väljalase sisaldab uusi võimalusi, näiteks võimalus suurendada PDF-dokumente, Active-põhine tahvelseadmete nitur, PDF-filmide parem toetus, vaatevälja liikumise sünkroonis valikuga, annotatsioonide muutmise täiustused. Kõigil, kes kasutavad Okulari, on soovitatav uuendada versiooni 0.16 peale.
