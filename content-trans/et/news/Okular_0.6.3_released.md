---
date: 2008-04-02
title: Ilmus Okular 0.6.3
---
KDE 4.0 seeria kolmas hooldusväljalase sisaldab Okulari 0.6.3. See sisaldab mõningaid veaparandusi, nt. paremat meetodit tuvastamaks teksti asukohta PDF-dokumendis, ning mitmeid parandusi annoteerimise süsteemis ja sisukordades. Kõiki parandatud vigu näeb muutuste logis <a href="http://www.kde.org/announcements/changelogs/changelog4_0_2to4_0_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0_2to4_0_3.php</a>
