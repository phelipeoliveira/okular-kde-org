---
date: 2017-08-17
title: Rilasciato Okular 1.2
---
La versione 1.2 di Okular è stato rilasciata insieme a KDE Applications 17.08. Questo rilascio introduce correzioni minori e miglioramenti. Puoi verificare le novità di versione all'indirizzo <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.08.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.08.0#okular</a>. È un aggiornamento consigliato per chiunque utilizzi Okular.
