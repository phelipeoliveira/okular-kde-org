---
date: 2020-12-10
title: Rilasciato Okular 20.12
---
È stata rilasciata la versione 20.12 di Okular. Questo rilascio introduce varie correzioni minori e aggiunte di funzionalità dappertutto. Puoi verificare le novità di versione all'indirizzo <a href='https://kde.org/announcements/changelog-releases.php?version=20.12.0#okular'>https://kde.org/announcements/changelog-releases.php?version=20.12.0#okular</a>. È un aggiornamento consigliato per chiunque utilizzi Okular.
