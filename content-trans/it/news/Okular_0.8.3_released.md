---
date: 2009-05-06
title: Rilasciato Okular 0.8.3
---
Il terzo rilascio di manutenzione della serie KDE 4.2 comprende Okular 0.8.3. Non contiene molte novità per Okular, l'unico cambiamento rilevante riguarda la sicurezza maggiore dei thread durante la generazione delle immagini pagina di documento XPS. Tutti i problemi risolti sono consultabili in <a href="http://www.kde.org/announcements/changelogs/changelog4_2_2to4_2_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_2to4_2_3.php</a>
