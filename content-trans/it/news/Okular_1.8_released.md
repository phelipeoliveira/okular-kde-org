---
date: 2019-08-15
title: Rilasciato Okular 1.8
---
La versione 1.8 di Okular è stato rilasciata insieme con KDE Applications 19.08. Questo rilascio introduce la funzione d'impostazione delle Annotazioni con estremità, oltre ad altre varie correzioni e piccole funzionalità. Puoi verificare le novità di versione all'indirizzo <a href='https://www.kde.org/announcements/fulllog_applications-aether.php?version=19.08.0#okular'>https://www.kde.org/announcements/fulllog_applications-aether.php?version=19.08.0#okular</a>. È un aggiornamento consigliato per chiunque utilizzi Okular.
