---
date: 2009-06-03
title: Rilasciato Okular 0.8.4
---
Il quarto rilascio di manutenzione della serie KDE 4.2, comprende Okular 0.8.4. Sono incluse alcune correzioni nei documenti di OpenDocument Text, un paio di correzioni dei crash e qualche piccola correzione degli errori nell'interfaccia. Tutti i problemi risolti sono consultabili in <a href="http://www.kde.org/announcements/changelogs/changelog4_2_3to4_2_4.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_3to4_2_4.php</a>
