---
date: 2018-12-13
title: Rilasciato Okular 1.6
---
La versione 1.6 di Okular è stato rilasciata insieme con KDE Applications 18.12. Questo rilascio introduce il nuovo strumento di annotazione Typewriter, oltre ad altre varie correzioni e piccole funzionalità. Puoi verificare le novità di versione all'indirizzo <a href='https://www.kde.org/announcements/fulllog_applications-aether.php?version=18.12.0#okular'>https://www.kde.org/announcements/fulllog_applications-aether.php?version=18.12.0#okular</a>. È un aggiornamento consigliato per chiunque utilizzi Okular.
