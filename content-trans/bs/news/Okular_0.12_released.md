---
date: 2011-01-26
title: Okular 0.12 pušten
---
Verzija 0.12 Okulara je puštena zajedno sa KDE 4.6 aplikacijom. Ovo izdanje predstavlja mala poboljšanja i nove mogućnosti i  preporučena je nadogradnja za sve korisnike Okulara.
