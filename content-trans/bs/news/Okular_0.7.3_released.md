---
date: 2008-11-04
title: Okular 0.7.3 pušten
---
Treće izdanje KDE 4.1 serije uključuje Okular 0.7.3. Ono uključuje neke minorne popravke u korisničkom sučelju i u pretraživanju teksta. Sve riješene probleme vezane za ovu temu možete pronaći na <a href="http://www.kde.org/announcements/changelogs/changelog4_1_2to4_1_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1_2to4_1_3.php</a>
