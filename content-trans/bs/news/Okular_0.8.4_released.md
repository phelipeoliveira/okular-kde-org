---
date: 2009-06-03
title: Okular 0.8.4 pušten
---
Četvrto izdanje KDE 4.2 serije uključuje Okular 0.8.4. To uključuje i neke ispravke u OtvoriDokument tekstnim dokumentima, nekoliko popravki krahiranja i nekoliko malih popravki u sučelju. Sve riješene probleme vezane za ovu temu možete pročitati na <a href="http://www.kde.org/announcements/changelogs/changelog4_2_3to4_2_4.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_3to4_2_4.php</a>
