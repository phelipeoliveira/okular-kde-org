---
date: 2014-12-17
title: Okular 0.21 veröffentlicht
---
Die Version 0.21 von Okular wurde zusammen mit den KDE-Anwendungen 4.12 veröffentlicht. Diese Veröffentlichung enthält neue Funktionen wie umgekehrte Suche mit latex-synctex in dvi und kleinere Fehlerkorrekturen. Okular 0.21 ist eine empfohlene Aktualisierung für jeden, der Okular benutzt.
