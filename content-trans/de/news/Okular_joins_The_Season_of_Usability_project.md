---
date: 2007-01-31
title: Okular nimmt am Projekt „The Season of Usability“ teil
---
Das Okular-Team freut sich, ankündigen zu können, dass Okular eine der Anwendungen ist, die für das Projekt „<a href="http://www.openusability.org/season/0607/">Season of Usability</a>“ ausgewählt wurde, das von den Usability-Experten von <a href="http://www.openusability.org">OpenUsability</a> veranstaltet wird. Wir heißen Sharad Baliyan im Team willkommen und danken Florian Graessle und Pino Toscano für ihre ununterbrochene Arbeit, um Okular zu verbessern.
