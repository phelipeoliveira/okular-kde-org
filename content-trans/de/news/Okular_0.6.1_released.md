---
date: 2008-02-05
title: Okular 0.6.1 veröffentlicht
---
Die erste Wartungsversion der Reihe KDE 4.0 enthält Okular 0.6.1. Diese Version bringt eine ganze Reihe Korrekturen. So werden z. B. Dateien nicht noch einmal heruntergeladen, wenn sie gespeichert werden und die Bedienbarkeit wurde verbessert.Eine Liste aller behobener Probleme finden Sie <a href="http://www.kde.org/announcements/changelogs/changelog4_0to4_0_1.php#okular">hier</a>.
