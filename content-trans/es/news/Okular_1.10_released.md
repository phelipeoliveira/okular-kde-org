---
date: 2020-04-23
title: Okular 1.10 publicado
---
La versión 1.10 de Okular ha sido publicada. Esta versión introduce desplazamiento cinético, mejoras en la gestión de pestañas, mejoras en la interfaz móvil y varias correcciones y funcionalidades menores. Puede consultar el registro de cambios completo en <a href='https://kde.org/announcements/changelog-releases.php?version=20.04.0#okular'>https://kde.org/announcements/changelog-releases.php?version=20.04.0#okular</a>. Okular 1.10 es una actualización recomendada para todos los usuarios de Okular.
