---
date: 2018-12-13
title: Okular 1.6 publicado
---
La versión 1.6 de Okular ha sido publicada junto al lanzamiento de las aplicaciones de KDE 18.12. Esta versión introduce la nueva herramienta de anotación de máquina de escribir entre otras correcciones y funcionalidades menores. Puede consultar el registro de cambios completo en <a href='https://www.kde.org/announcements/fulllog_applications- aether.php?version=18.12.0#okular'>https://www.kde.org/announcements/fulllog_applications-aether.php?version=18.12.0#okular</a>. Okular 1.6 es una actualización recomendada para todos los usuarios de Okular.
