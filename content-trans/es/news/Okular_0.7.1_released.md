---
date: 2008-09-03
title: Okular 0.7.1 publicado
---
El primer lanzamiento de mantenimiento de la serie KDE 4.1 incluye Okular 0.9.4, que contiene algunas soluciones a cuelgues entre otras correcciones menores. Puede ver los problemas solucionados en <a href="http://www.kde.org/announcements/changelogs/changelog4_1to4_1_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1to4_1_1.php</a>
