---
date: 2010-08-10
title: Okular 0.11 publicado
---
La versión 0.11 de Okular ha sido publicada junto al lanzamiento de las aplicaciones de KDE 4.5. Esta versión incluye correcciones y características menores, y es una actualización recomendada para todos los usuarios de Okular.
