---
date: 2020-08-13
title: Okular 1.11 publicado
---
La versión 1.11 de Okular ha sido publicada. Esta versión introduce una nueva interfaz para anotaciones y varias correcciones y funcionalidades menores. Puede consultar el registro de cambios completo en <a href='https://kde.org/announcements/changelog-releases.php?version=20.08.0#okular'>https://kde.org/announcements/changelog-releases.php?version=20.08.0#okular</a>. Okular 1.11 es una actualización recomendada para todos los usuarios de Okular.
