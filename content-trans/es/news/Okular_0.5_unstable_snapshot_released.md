---
date: 2006-08-27
title: Snapshot inestable de Okular 0.5 publicado
---
El equipo de Okular se enorgullece de anunciar el lanzamiento de una snapshot de Okular que compila con la <a href="http://dot.kde.org/1155935483/">snapshot KDE 4 «Krash»</a>. Esta snapshot aún no es completamente funcional, ya que todavía tenemos muchas cosas que pulir y finalizar, pero puede probarla y enviar sus comentarios y sugerencias. Puede encontrar el paquete de la snapshot en <a href="ftp://ftp.kde.org/pub/kde/unstable/apps/KDE4.x/graphics/okular-0.5.tar.bz2">ftp://ftp.kde.org/pub/kde/unstable/apps/KDE4.x/graphics/okular-0.5.tar.bz2</a>. Consulte la página de <a href="download.php">descargas</a> para asegurarse de que tiene todas las bibliotecas necesarias.
